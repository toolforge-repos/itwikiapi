# -*- coding: utf-8 -*-
#
# Copyright (C) 2024 Sakretsu and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import httpx
import itwikirevisions
import itwikivotingeligibility

from fastapi import FastAPI, HTTPException, Path
from fastapi.middleware.cors import CORSMiddleware
from typing import Annotated

app = FastAPI()

origins = [
    "https://it.wikipedia.org",
    "https://it.m.wikipedia.org"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins
)

@app.get("/v1/revisions")
async def revisions(
    templatenames: str,
    pageid: int | None = None,
    revid: int | None = None,
    rvprop: str | None = None
):
    return itwikirevisions.getData(pageid, revid, rvprop, templatenames)

@app.get("/v1/users/{user}/voting-eligibility/{date}")
async def voting_eligibility(
    user: str,
    date: Annotated[datetime.date, Path()]
):
    return itwikivotingeligibility.getData(user, date)
