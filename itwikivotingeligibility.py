# -*- coding: utf-8 -*-
#
# Copyright (C) 2024 Sakretsu and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import pywikibot
import re
import requests

from abc import ABC, abstractmethod
from datetime import timedelta
from dateutil.relativedelta import relativedelta
from pywikibot.data import api
from threading import Thread
from zoneinfo import ZoneInfo


config = None

def loadConfig():
    global config
    session = requests.Session()
    url = 'https://it.wikipedia.org/w/index.php'
    params = {
        'title': 'MediaWiki:Requisiti di voto - autoedits.json',
        'action': 'raw',
        'ctype': 'application/json'
    }
    response = session.get(url=url, params=params)

    try:
        config = response.json()
    except Exception:
        config = {}

config_thread = Thread(target=loadConfig)
config_thread.start()

class Requirement(ABC):

    def __init__(self):
        self.requirementsatisfied = False

    @abstractmethod
    def verify(self):
        pass

class RegistrationRequirement(Requirement):

    def __init__(self):
        super().__init__()
        self.timestamp = None

    def verify(self, site, username):
        user = pywikibot.User(site, username)

        if user.isRegistered():
            self.requirementsatisfied = True
        else:
            return

        registration_date = user.registration()

        if registration_date:
            self.timestamp = registration_date.isoformat()

class ActivityRequirement(Requirement):

    def __init__(self):
        super().__init__()
        self.namespace = 'all'
        self.editcount = 0
        self.threshold = None
        self.interval = {
            'starttimestamp': '2001-05-10T22:00:00Z',
            'endtimestamp': pywikibot.Timestamp.utcnow().isoformat()
        }

    def _isContribValid(self, contrib):
        config_thread.join()

        for autoedit in config.values():
            if ('tags' in autoedit and
                    'tags' in contrib and
                    set(autoedit['tags']).intersection(contrib['tags'])):
                return False

            if ('regex' in autoedit and
                    'comment' in contrib and
                    re.search(autoedit['regex'], contrib['comment'])):
                return False

        return True

    def verify(self, site, username, threshold, min_date=None, max_date=None):
        self.threshold = threshold

        if min_date:
            self.interval['starttimestamp'] = (
                min_date.astimezone(ZoneInfo('UTC')))

        if max_date:
            self.interval['endtimestamp'] = (
                max_date.astimezone(ZoneInfo('UTC')))

        if self.threshold <= 0:
            return

        parameters = {
            'ucdir': 'newer',
            'ucprop': 'title|timestamp|comment|tags',
            'ucstart': min_date,
            'ucend': max_date,
            'ucuser': username
        }
        gen = api.ListGenerator('usercontribs',
                                site=site, parameters=parameters)

        firstcontrib = None

        for contrib in gen:
            if self._isContribValid(contrib) is False:
                continue

            self.editcount += 1

            if self.editcount == 1:
                firstcontrib = contrib

            if self.editcount == self.threshold:
                self.interval['starttimestamp'] = firstcontrib['timestamp']
                self.interval['endtimestamp'] = contrib['timestamp']
                self.requirementsatisfied = True
                break

def addRequirement(key, requirement_class, requirements, **kwargs):
    requirement = requirement_class()
    requirement.verify(**kwargs)
    requirements[key] = vars(requirement)

def subtract_delta(date, months=None, ms=None):
    if months:
        date -= relativedelta(months=months)

    if ms:
        date -= timedelta(milliseconds=ms)

    return date

def getData(username, date):
    date = pywikibot.Timestamp(date.year, date.month, date.day,
                               tzinfo=ZoneInfo('Europe/Rome'))
    site = pywikibot.Site('wikipedia:it')
    data = {
        'username': username,
        'eligible': False,
        'requirements': {
            'registration': vars(RegistrationRequirement()),
            'oldactivity': vars(ActivityRequirement()),
            'recentactivity': vars(ActivityRequirement())
        }
    }
    requirements = data['requirements']

    threads = [
        Thread(target=addRequirement,
               args=('registration', RegistrationRequirement, requirements),
               kwargs={'site': site, 'username': username}),
        Thread(target=addRequirement,
               args=('oldactivity', ActivityRequirement, requirements),
               kwargs={'site': site, 'username': username, 'threshold': 600,
                       'max_date': subtract_delta(date, months=3, ms=1)}),
        Thread(target=addRequirement,
               args=('recentactivity', ActivityRequirement, requirements),
               kwargs={'site': site, 'username': username, 'threshold': 50,
                       'min_date': subtract_delta(date, months=6),
                       'max_date': subtract_delta(date, ms=1)})
    ]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    unmet_requirements = [r for r in data['requirements'].values()
                          if r['requirementsatisfied'] is False]

    if len(unmet_requirements) == 0:
        data['eligible'] = True

    return data
