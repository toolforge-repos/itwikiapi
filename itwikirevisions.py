# -*- coding: utf-8 -*-
#
# Copyright (C) 2024 Sakretsu and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import requests
import wikitextparser

from pywikibot import textlib


def toJSON(template):
    json = {
        'name': template.normal_name(capitalize=True),
        'args': {},
        'span': [
            template.span[0],
            template.span[1]
        ]
    }

    for arg in template.arguments:
        normalized_name = textlib.removeDisabledParts(arg.name).strip()
        normalized_name = ' '.join(normalized_name.split())
        json['args'][normalized_name] = {
            'name': arg.name,
            'value': arg.value,
            'span': [
                arg.span[0],
                arg.span[1]
            ]
        }

    return json

def findTemplates(template_names, parsed_text):
    templates = []

    for template in parsed_text.templates:
        if template.normal_name(capitalize=True) in template_names.split('|'):
            templates.append(template)

    return templates

def getData(pageid, revid, rvprop, templatenames):
    session = requests.Session()
    url = 'https://it.wikipedia.org/w/api.php'
    params = {
        'pageids': pageid,
        'revids': revid,
        'action': 'query',
        'format': 'json',
        'formatversion': 2,
        'prop': 'revisions',
        'rvprop': 'content|' + rvprop if rvprop else 'content',
        'rvslots': 'main'
    }
    response = session.get(url=url, params=params)
    data = response.json()

    try:
        current_revision = data['query']['pages'][0]['revisions'][0]
        parsed_text = wikitextparser.parse(
            current_revision['slots']['main']['content'])
        templates = findTemplates(templatenames, parsed_text)
        current_revision['templates'] = [toJSON(t) for t in templates]

        if rvprop is None or 'content' not in rvprop:
            del current_revision['slots']
    except KeyError:
        pass

    return data
